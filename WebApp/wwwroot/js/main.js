﻿function run(url,document) {
    $.getJSON(url, function (data) {
        WriteDocFile(document, data);
    }).fail(function () {
        alert("ошибка получения данных, проверьте заполняемость требуемых полей");
    });
    return false;
}

function WriteDocFile(file, data) {
    var loadFile = function (url, callback) {
        JSZipUtils.getBinaryContent(url, callback);
    }
    loadFile(file, function (err, content) {
        var doc = new Docxgen(content);
        doc.setData(data);
        doc.render();
        var output = doc.getZip().generate({ type: "blob" }); 
        saveAs(output, "output.docx");
    });
}