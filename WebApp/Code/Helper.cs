﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNet.Mvc.ModelBinding;

namespace WebApp.Code
{
    public static class Helper
    {
        /// <summary>
        /// для мапера в обе стороны
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <typeparam name="TDestination"></typeparam>
        /// <param name="mappingExpression"></param>
        /// <returns></returns>
        public static IMappingExpression<TDestination, TSource> BothWays<TSource, TDestination>
        (this IMappingExpression<TSource, TDestination> mappingExpression)
        {
            return Mapper.CreateMap<TDestination, TSource>();
        }



        /// <summary>
        /// ext метод для fast сравнивания guild
        /// </summary>
        /// <param name="val"></param>
        /// <param name="alt"></param>
        /// <returns></returns>
        public static bool Eq(this Guid val, Guid alt)
        {
            var a = BitConverter.ToInt64(val.ToByteArray(), 0);
            var b = BitConverter.ToInt64(alt.ToByteArray(), 0);
            return a == b;
        }


        /// <summary>
        /// показать ошибки в "чистом виде"
        /// </summary>
        /// <param name="modelState"></param>
        /// <returns></returns>
        public static IEnumerable<string> GetErrors(this ModelStateDictionary modelState)
        {
            var errors = (from er in modelState
                          let k = er.Key
                          where er.Value != null && er.Value.Errors.Any()
                          select $"{k} {er.Value.Errors.First().ErrorMessage}")
                .Distinct()
                .ToList();
            return errors;
        }
     

        /// <summary>
        /// все будет хорошо!!!!
        /// рефлекция - не работает в dnx
        /// 
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        
        public static string Safe(object val)
        {
            if (val == null) return string.Empty;

            var type = val.GetType().ToString();
            if (type.Equals("System.int32", StringComparison.OrdinalIgnoreCase))
            {
                return ((int)val).ToString();
            }
            else if (type.Equals("System.boolean", StringComparison.OrdinalIgnoreCase))
            {
                return ((bool)val) ? "да" : "нет";
            }
            else if (type.Equals("System.double", StringComparison.OrdinalIgnoreCase))
            {
                return ((double)val).ToString("0.000");
            }
            else if (type.Equals("System.decimal", StringComparison.OrdinalIgnoreCase))
            {
                return ((decimal)val).ToString("0.000");
            }
            else if (type.Equals("System.dateTime", StringComparison.OrdinalIgnoreCase))
            {
                return ((DateTime)val).ToString("dd.MM.yyyy HH:mm");
            }
            else if (type.Equals("System.string", StringComparison.OrdinalIgnoreCase))
            {
                return ((string)val);
            }
            return "пожалуйста, заполните это";
        }

        public static string Decode64Tostring(string text)
        {
            var row = string.Empty;
            if (!string.IsNullOrWhiteSpace(text))
            {

                var data = Convert.FromBase64String(text);
                row = Encoding.UTF8.GetString(data);

            }
            return row;
        }

        public static string Encode64Tostring(string text)
        {
            var bytes = Encoding.UTF8.GetBytes(text);
            var row = Convert.ToBase64String(bytes);
            return row;
        }



    }
}
